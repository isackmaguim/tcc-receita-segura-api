package br.com.receitaseguraapi.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Genero {

	MASCULINO(1, "Masculino"),
	FEMININO(2, "Feminino");
	
	private int codigo;
	private String descricao;
	
	public static Genero toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}
		for (Genero x : Genero.values()) {
			if(codigo.equals(x.getCodigo())) {
				return x;
			}
			
		}
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
}
