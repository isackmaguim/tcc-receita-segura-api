package br.com.receitaseguraapi.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PermissaoDeAcesso {

	CADASTRA_USUARIO(10, "ROLE_CADASTRA_USUARIO"),
	PESQUISA_USUARIO(11, "ROLE_PESQUISA_USUARIO"),
	REMOVE_USUARIO(12, "ROLE_REMOVE_USUARIO"),
	EDITA_USUARIO(13, "ROLE_EDITA_USUARIO"),
	RELATORIO_USUARIO(14, "ROLE_EDITA_USUARIO"),
	
	CADASTRA_MEDICO(20, "ROLE_CADASTRA_MEDICO"),
	REMOVE_MEDICO(21, "ROLE_REMOVE_MEDICO"),
	EDITA_MEDICO(22, "ROLE_EDITA_MEDICO"),
	PESQUISA_MEDICO(23, "ROLE_PESQUISA_MEDICO"),
	RELATORIO_MEDICO(24, "ROLE_RELATORIO_MEDICO"),
	
	CADASTRA_INSTITUICAO(30, "ROLE_CADASTRA_INSTITUICAO"),
	REMOVE_INSTITUICAO(31, "ROLE_REMOVE_INSTITUICAO"),
	EDITA_INSTITUICAO(32, "ROLE_EDITA_INSTITUICAO"),
	PESQUISA_INSTITUICAO(33, "ROLE_PESQUISA_INSTITUICAO"),
	RELATORIO_INSTITUICAO(34, "ROLE_RELATORIO_INSTITUICAO"),	
	
	CADASTRA_ESPECIALIDADE(40, "ROLE_CADASTRA_ESPECIALIDADE"),
	REMOVE_ESPECIALIDADE(41, "ROLE_REMOVE_ESPECIALIDADE"),
	EDITA_ESPECIALIDADE(42, "ROLE_EDITA_ESPECIALIDADE"),
	PESQUISA_ESPECIALIDADE(43, "ROLE_PESQUISA_ESPECIALIDADE"),
	RELATORIO_ESPECIALIDADE(44, "ROLE_RELATORIO_ESPECIALIDADE"),
	
	CADASTRA_PACIENTE(50, "ROLE_CADASTRA_PACIENTE"),
	REMOVE_PACIENTE(51, "ROLE_REMOVE_PACIENTE"),
	EDITA_PACIENTE(52, "ROLE_EDITA_PACIENTE"),
	PESQUISA_PACIENTE(53, "ROLE_PESQUISA_PACIENTE"),
	RELATORIO_PACIENTE(54, "ROLE_RELATORIO_PACIENTE"),
	
	CADASTRA_CONSELHO(60, "ROLE_CADASTRA_CONSELHO"),
	REMOVE_CONSELHO(61, "ROLE_REMOVE_CONSELHO"),
	EDITA_CONSELHO(62, "ROLE_EDITA_CONSELHO"),
	PESQUISA_CONSELHO(63, "ROLE_PESQUISA_CONSELHO"),
	RELATORIO_CONSELHO(64, "ROLE_RELATORIO_CONSELHO"),

	CADASTRA_ATESTADO(70, "ROLE_CADASTRA_ATESTADO"),
	REMOVE_ATESTADO(71, "ROLE_REMOVE_ATESTADO"),
	EDITA_ATESTADO(72, "ROLE_EDITA_ATESTADO"),
	PESQUISA_ATESTADO(73, "ROLE_PESQUISA_ATESTADO"),
	RELATORIO_ATESTADO(74, "ROLE_RELATORIO_ATESTADO"),
	
	PESQUISA_CID(83, "ROLE_PESQUISA_CID"),
	RELATORIO_CID(84, "ROLE_RELATORIO_CID"),

	PESQUISA_MEDICAMENTO(93, "ROLE_PESQUISA_MEDICAMENTO"),
	RELATORIO_MEDICAMENTO(94, "ROLE_RELATORIO_MEDICAMENTO"),
	
	CADASTRA_RECEITA(100, "ROLE_CADASTRA_RECEITA"),
	REMOVE_RECEITA(101, "ROLE_REMOVE_RECEITA"),
	EDITA_RECEITA(102, "ROLE_EDITA_RECEITA"),
	PESQUISA_RECEITA(103, "ROLE_PESQUISA_RECEITA"),
	RELATORIO_RECEITA(104, "ROLE_RELATORIO_RECEITA");
	
	private int codigo;
	private String descricao;
	
	public static PermissaoDeAcesso toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}
		for (PermissaoDeAcesso x : PermissaoDeAcesso.values()) {
			if(codigo.equals(x.getCodigo())) {
				return x;
			}
		}
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
	
}
