package br.com.receitaseguraapi.model.general;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.receitaseguraapi.model.enums.StatusDoc;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "tipo", discriminatorType = DiscriminatorType.STRING)
public class Documento extends Sequencial implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "medico_fk")
	private Medico medico;

	@ManyToOne
	@JoinColumn(name = "paciene_fk")
	private Paciente paciente;

	private String assinatura;

	private String chaveAutenticacao;

	private Integer statusDoc;

	private String autorizacao;

	public Documento(Medico medico, Paciente paciente, String assinatura,
			String chaveAutenticacao, StatusDoc statusDoc, String autorizacao) {
		super();
		this.medico = medico;
		this.paciente = paciente;
		this.assinatura = assinatura;
		this.chaveAutenticacao = chaveAutenticacao;
		this.statusDoc = statusDoc.getCodigo();
		this.autorizacao = autorizacao;
	}
	
	@NonNull
	public StatusDoc getStatusDoc() {

		return StatusDoc.toEnum(statusDoc);

	}

	@NonNull
	public void setStatusDoc(StatusDoc tipoDoc) {

		this.statusDoc = tipoDoc.getCodigo();

	}
}
