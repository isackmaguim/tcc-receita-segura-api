package br.com.receitaseguraapi.model.general;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Medicamento extends Sequencial implements Serializable {
	 
	private static final long serialVersionUID = 1L;

	@NotNull
	private String principioAtivo;
	@NotNull
	private String cnpj;
	@NotNull
	private String laboratorio;
	@NotNull
	private String produto;
	@NotNull
	private String apresentacao;
	@NotNull
	private String classeTerapeutica;
	@NotNull
	private String tipoProduto;
	@NotNull
	private boolean restricaoHospitalar;
	@NotNull
	private String tarja;
	
	
}
