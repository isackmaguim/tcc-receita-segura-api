package br.com.receitaseguraapi.model.general;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.receitaseguraapi.model.enums.TipoInstituicao;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Instituicao extends Sequencial {

	private String cnpj;
	private String razaoSocial;
	private Integer tipoInstituicao;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "instituicao_id", referencedColumnName = "id")
	private Endereco endereco;

	public Instituicao(String cnpj, String razaoSocial, TipoInstituicao tipoInstituicao) {
		super();
		this.cnpj = cnpj;
		this.razaoSocial = razaoSocial;
		this.tipoInstituicao = tipoInstituicao.getCodigo();
	}

	@NonNull
	public TipoInstituicao getTipoInstituicao() {

		return TipoInstituicao.toEnum(tipoInstituicao);

	}

	@NonNull
	public void setTipoInstituicao(TipoInstituicao tipoInstituicao) {

		this.tipoInstituicao = tipoInstituicao.getCodigo();

	}

}
