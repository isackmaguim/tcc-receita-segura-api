package br.com.receitaseguraapi.model.general;

import java.io.Serializable;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Especialidade extends Sequencial implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nomeEspecialidade;

	public Especialidade(String nomeEspecialidade) {
		super();
		this.nomeEspecialidade = nomeEspecialidade;

	}

}
