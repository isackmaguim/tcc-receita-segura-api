package br.com.receitaseguraapi.model.general;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import br.com.receitaseguraapi.model.enums.StatusDoc;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@PrimaryKeyJoinColumn(referencedColumnName = "id")
@DiscriminatorValue(value = "atestado")
public class Atestado extends Documento implements Serializable {

	private static final long serialVersionUID = 1L;
		
	private Boolean permiteInformarCid;

	@ManyToOne
	@JoinColumn(name = "cid_fk")
	private Cid cidFk;
	private Integer diasAfastamento;

	public Atestado(Medico medico, Paciente paciente, String assinatura,
			String chaveAutenticacao, StatusDoc statusDoc, String autorizacao, Boolean permiteInformarCid, Cid cidFk, Integer diasAfastamento) {
		super(medico, paciente, assinatura, chaveAutenticacao, statusDoc, autorizacao);
		this.permiteInformarCid = permiteInformarCid;
		this.cidFk = cidFk;
		this.diasAfastamento = diasAfastamento;
	}
	
}
