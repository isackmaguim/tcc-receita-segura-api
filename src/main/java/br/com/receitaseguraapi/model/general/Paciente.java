package br.com.receitaseguraapi.model.general;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import br.com.receitaseguraapi.model.enums.TipoPessoa;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@PrimaryKeyJoinColumn(referencedColumnName = "id")
@DiscriminatorValue("pessoa_id")
public class Paciente extends Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	public Paciente(String nome, String cpf, String rg, TipoPessoa tipoPessoa) {
		super(nome, cpf, rg, tipoPessoa);
	
	}

}
