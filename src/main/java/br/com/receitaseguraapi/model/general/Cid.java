package br.com.receitaseguraapi.model.general;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Cid extends Sequencial {

	@NotNull
	@Size(min = 3, max = 3)
	private String cid;

	@NotNull
	@Size(min = 5, max = 255)
	private String descricao;

	
}
