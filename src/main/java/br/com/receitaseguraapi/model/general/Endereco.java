package br.com.receitaseguraapi.model.general;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Endereco extends Sequencial implements Serializable {

	private static final long serialVersionUID = 1L;

	private String logradouro;
	private String bairro;
	private String cep;
	private String cidade;
	private String uf;
	private String numeroEndereco;
	private String complemento;
	
	@JsonIgnore
	@OneToOne(mappedBy = "endereco")
    private Instituicao instituicao;

	@JsonIgnore
	@OneToOne(mappedBy = "endereco")
    private Pessoa pessoa;

}
