package br.com.receitaseguraapi.model.dto.general;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import br.com.receitaseguraapi.model.general.Prescricao;
import br.com.receitaseguraapi.model.general.Receita;
import br.com.receitaseguraapi.utils.Conversor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReceitaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Date dataCadastro;
	private String nomePaciente;
	private String cpfPaciente;
	private String rgPaciente;
	private Date dataValidade;
	private String localidade;
	private String autorizacao;
	private String medico;
	private String crm;
	private String assinatura;
	private String chaveAutenticacao;
	private List<PrescricaoDTO> prescricoes;
	
	public ReceitaDTO(Receita obj) {

		id = obj.getId();
		dataCadastro = Conversor.converterParaData(obj.getDataCadastro());
		nomePaciente = obj.getPaciente().getNome();
		cpfPaciente = obj.getPaciente().getCpf();
		rgPaciente = obj.getPaciente().getRg();
		dataValidade = Conversor.converterParaData(obj.getDataValidade());
		localidade = obj.getMedico().getEndereco().getCidade() + "-" + obj.getMedico().getEndereco().getUf();
		autorizacao = obj.getAutorizacao();
		medico = obj.getMedico().getNome();
		crm = obj.getMedico().getCrm();
		assinatura = obj.getAssinatura();
		chaveAutenticacao = obj.getChaveAutenticacao();
		prescricoes = converteParPrescricaoDTO(obj.getPrescricoes());
		
	}

	private List<PrescricaoDTO> converteParPrescricaoDTO(List<Prescricao> prescricao) {
		
		List<PrescricaoDTO> prescricaoDTO = prescricao.stream().map(obj -> new PrescricaoDTO(obj)).collect(Collectors.toList());
		return prescricaoDTO;
	}

}
