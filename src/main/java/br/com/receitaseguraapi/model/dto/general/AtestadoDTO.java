package br.com.receitaseguraapi.model.dto.general;

import java.io.Serializable;
import java.util.Date;

import br.com.receitaseguraapi.model.general.Atestado;
import br.com.receitaseguraapi.utils.Conversor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AtestadoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Date dataCadastro;
	private String nomePaciente;
	private String cpfPaciente;
	private String rgPaciente;
	private Integer diasAfastamento;
	private String cid;
	private String localidade;
	private String autorizacao;
	private String medico;
	private String crm;
	private String assinatura;
	private String chaveAutenticacao;
	
	public AtestadoDTO(Atestado obj) {

		id = obj.getId();
		dataCadastro = Conversor.converterParaData(obj.getDataCadastro());
		nomePaciente = obj.getPaciente().getNome();
		cpfPaciente = obj.getPaciente().getCpf();
		rgPaciente = obj.getPaciente().getRg();
		diasAfastamento = obj.getDiasAfastamento();
		cid = obj.getCidFk().getCid();
		localidade = obj.getMedico().getEndereco().getCidade() + "-" + obj.getMedico().getEndereco().getUf();
		autorizacao = obj.getAutorizacao();
		medico = obj.getMedico().getNome();
		crm = obj.getMedico().getCrm();
		assinatura = obj.getAssinatura();
		chaveAutenticacao = obj.getChaveAutenticacao();
	}

}
