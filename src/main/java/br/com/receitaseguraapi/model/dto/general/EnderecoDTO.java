package br.com.receitaseguraapi.model.dto.general;

import br.com.receitaseguraapi.model.general.Endereco;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class EnderecoDTO {

	private String logradouro;
	private String bairro;
	private String cep;
	private String cidade;

	public EnderecoDTO(Endereco obj) {

		logradouro = obj.getLogradouro();
		bairro = obj.getBairro();
		cep = obj.getCep();
		cidade = obj.getCidade();

	}

}
