package br.com.receitaseguraapi.model.usuario;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import br.com.receitaseguraapi.model.enums.NivelDeAcesso;
import br.com.receitaseguraapi.model.enums.PermissaoDeAcesso;
import br.com.receitaseguraapi.model.enums.StatusUsuario;
import br.com.receitaseguraapi.model.enums.TipoUsuario;
import br.com.receitaseguraapi.model.general.Instituicao;
import br.com.receitaseguraapi.model.general.Sequencial;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonAutoDetect
public class Usuario extends Sequencial implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "The matricula parameter", required = true)
	@Column(unique = true, nullable = false)
	private String matricula;

	@ApiModelProperty(notes = "The password parameter", required = true)
	@Length(min = 1, message = "O tamanho deve um minimo de 1 caracter")
	@Column(name = "password", nullable = true, length = 250)
	private String senha;
	private String email;
	private String nomeUsuario;
	private Integer statusUsuario;
	private Integer tentativasDeAcesso;
	private Integer nivelDeAcesso;
	private boolean primeiroAcesso;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "permissao_de_acesso")
	@Cascade(value=CascadeType.ALL)
	private Set<Integer> permissoes = new HashSet<>();

	private Integer tipoUsuario;

	@ManyToOne
	@JoinColumn(name = "instituicao_id")
	private Instituicao instituicao;

	
	public Usuario(String matricula, String senha, String email, String nomeUsuario, 
			StatusUsuario statusUsuario, Integer tentativasDeAcesso, boolean primeiroAcesso,
			NivelDeAcesso nivelDeAcesso, TipoUsuario tipoUsuario, Instituicao instituicao) {
		super();
		this.matricula = matricula;
		this.senha = senha;
		this.email = email;
		this.nomeUsuario = nomeUsuario;
		this.statusUsuario = statusUsuario.getCodigo();
		this.tentativasDeAcesso = tentativasDeAcesso;
		this.primeiroAcesso = primeiroAcesso;
		this.nivelDeAcesso = nivelDeAcesso.getCodigo();
		this.tipoUsuario = tipoUsuario.getCodigo();
		this.instituicao = instituicao;
	}

	@NonNull
	public StatusUsuario getStatusUsuario() {
		return StatusUsuario.toEnum(statusUsuario);
	}

	@NonNull
	public void setStatusUsuario(StatusUsuario statusUsuario) {
		this.statusUsuario = statusUsuario.getCodigo();
	}

	@NonNull
	public Set<PermissaoDeAcesso> getPermissaoDeAcesso() {
		return permissoes.stream().map(x -> PermissaoDeAcesso.toEnum(x)).collect(Collectors.toSet());
	}

	@NonNull
	public void addPermissao(PermissaoDeAcesso permissaoDeAcesso) {
		permissoes.add(permissaoDeAcesso.getCodigo());
	}

	@NonNull
	public NivelDeAcesso getNivelDeAcesso() {
		return NivelDeAcesso.toEnum(nivelDeAcesso);
	}

	@NonNull
	public void setNivelDeAcesso(NivelDeAcesso nivelDeAcesso) {
		this.nivelDeAcesso = nivelDeAcesso.getCodigo();
	}
	
	@NonNull
	public TipoUsuario getTipoUsuario() {
		return TipoUsuario.toEnum(tipoUsuario);
	}

	@NonNull
	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario.getCodigo();
	}
	
	
}