package br.com.receitaseguraapi.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.receitaseguraapi.model.usuario.Usuario;
import br.com.receitaseguraapi.repository.UsuarioRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String matricula) {

		Optional<Usuario> usuarioOptional = usuarioRepository.findByMatricula(matricula);
		Usuario usuario = usuarioOptional
				.orElseThrow(() -> new UsernameNotFoundException("Usuario e/ou senha incorretos !!"));

		return new UserSS(usuario.getId(), usuario.getMatricula(), usuario.getSenha(), usuario.getNomeUsuario(),
				usuario.getNivelDeAcesso(), usuario.isPrimeiroAcesso(), usuario.getPermissaoDeAcesso(),
				usuario.getInstituicao(), usuario.getTipoUsuario());

	}

}
