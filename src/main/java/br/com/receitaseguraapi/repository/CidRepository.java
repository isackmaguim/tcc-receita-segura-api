package br.com.receitaseguraapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.receitaseguraapi.model.general.Cid;

public interface CidRepository extends JpaRepository<Cid, Integer> {

}
