package br.com.receitaseguraapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.receitaseguraapi.model.general.Endereco;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

	@Transactional(readOnly=true)
	@Query(value = "select * from receitasegura.endereco u where u.cep = :cep", nativeQuery = true)
	public Optional<Endereco> findByCep(@Param("cep") String cep);


}
