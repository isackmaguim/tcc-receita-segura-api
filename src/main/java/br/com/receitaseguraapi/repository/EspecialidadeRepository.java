package br.com.receitaseguraapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.receitaseguraapi.model.general.Especialidade;

@Repository
public interface EspecialidadeRepository extends JpaRepository<Especialidade, Integer> {

	@Transactional(readOnly = true)
	@Query(value = "select * from receitasegura.especialidade u where u.id = :id or u.nome_especialidade like %:nomeEspecialidade%", nativeQuery = true)
	Page<Especialidade> findByEspecialidade(@Param("id") String id,
			@Param("nomeEspecialidade") String nomeEspecialidade, Pageable pageable);
	
	@Query(value = "select * from receitasegura.especialidade where id = :especialidade limit 1", nativeQuery = true)
	public Optional<Especialidade> buscarVagasTotais(@Param("especialidade") String especialidade);
	
	@Query(value = "SELECT distinct e.* FROM receitasegura.profissional u JOIN receitasegura.especialidade e "
			+ "ON u.especialidade_id = e.id WHERE u.tipo_profissional = :tipoProfissional "
			+ "AND u.status_profissional = :status AND e.classificacao = :classificacao", nativeQuery = true)
	public List<Especialidade> buscarEspecialidadesAtivas(
			@Param("tipoProfissional") String tipoProfissional,
			@Param("status") String status,
			@Param("classificacao") String classificacao);
	
}
