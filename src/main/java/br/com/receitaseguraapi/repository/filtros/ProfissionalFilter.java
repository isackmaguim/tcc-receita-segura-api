package br.com.receitaseguraapi.repository.filtros;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfissionalFilter {

	private String id;
	private String especialidade;
	private String nomeProfissional;
	private String instituicaoSaude;
	private String tipoProfissional;
	private String cpf;
	private String matricula;
	private String statusProfissional;
	private String dataConsulta;
	private String classificacao;
}
