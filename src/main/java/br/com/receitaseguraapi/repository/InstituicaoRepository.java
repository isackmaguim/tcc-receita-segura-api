package br.com.receitaseguraapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.receitaseguraapi.model.general.Instituicao;

@Repository
public interface InstituicaoRepository extends JpaRepository<Instituicao, Integer> {

	@Transactional(readOnly=true)
	public List<Instituicao> findByOrderByRazaoSocial();
	
	@Transactional(readOnly=true)
	public Optional<Instituicao> findById(String id);
	
	@Transactional(readOnly=true)
	@Query(value = "select * from receitasegura.instituicao_saude u where u.id = :id", nativeQuery = true)
	public Page<Instituicao> findByApenasId(@Param("id") String id, Pageable pageable);
	
	@Transactional(readOnly=true)
	@Query(value = "select * from receitasegura.instituicao_saude u where u.razao_social like %:razaoSocial%", nativeQuery = true)
	public Page<Instituicao> findByApenasRazaoSocial(@Param("razaoSocial") String razaoSocial, Pageable pageable);

	@Query(value = "SELECT * FROM receitasegura.instituicao_saude\n"
			+ "WHERE status_instituicao_saude = 1 order by razao_social", nativeQuery = true)
	public List<Instituicao> InstituicaoSaudeTodasDTO();

}
