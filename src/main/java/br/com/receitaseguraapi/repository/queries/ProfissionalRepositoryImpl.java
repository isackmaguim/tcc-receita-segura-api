package br.com.receitaseguraapi.repository.queries;

public class ProfissionalRepositoryImpl implements ProfissionalRepositoryQuery {
/*
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Profissional> filtrar(ProfissionalFilter profissionalFilter, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();

		CriteriaQuery<Profissional> criteria = builder.createQuery(Profissional.class);
		Root<Profissional> root = criteria.from(Profissional.class);

		Predicate[] predicates = criarRestricoes(profissionalFilter, builder, root);

		criteria.where(predicates);

		TypedQuery<Profissional> query = manager.createQuery(criteria);

		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(profissionalFilter));

	}

	private Predicate[] criarRestricoes(ProfissionalFilter profissionalFilter, CriteriaBuilder builder,
			Root<Profissional> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(profissionalFilter.getNomeProfissional())) {
			predicates.add(builder.like(root.get(Profissional_.nomeProfissional), "%" + profissionalFilter.getNomeProfissional() + "%"));
		}
		if (profissionalFilter.getEspecialidade() != null) {
			predicates.add(builder.equal(root.get(Profissional_.especialidade).get(Especialidade_.id), profissionalFilter.getEspecialidade()));
		}
		if (!StringUtils.isEmpty(profissionalFilter.getInstituicaoSaude())) {
			predicates.add(builder.equal(root.get(Profissional_.instituicao), profissionalFilter.getInstituicaoSaude()));
		}
		if (!StringUtils.isEmpty(profissionalFilter.getTipoProfissional())) {
			predicates.add(builder.equal((root.get(Profissional_.tipoProfissional)), profissionalFilter.getTipoProfissional()));
		}
		if (!StringUtils.isEmpty(profissionalFilter.getCpf())) {
			predicates.add(builder.equal(root.get(Profissional_.cpf), profissionalFilter.getCpf()));
		}
		if (!StringUtils.isEmpty(profissionalFilter.getMatricula())) {
			predicates.add(builder.equal(root.get(Profissional_.matricula), profissionalFilter.getMatricula()));
		}
		if (!StringUtils.isEmpty(profissionalFilter.getStatusProfissional())) {
			predicates.add(builder.equal(root.get(Profissional_.statusProfissional), profissionalFilter.getStatusProfissional()));
		}
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}

	private Long total(ProfissionalFilter profissionalFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Profissional> root = criteria.from(Profissional.class);

		Predicate[] predicates = criarRestricoes(profissionalFilter, builder, root);

		criteria.where(predicates);
		criteria.select(builder.count(root));
		
		
		return manager.createQuery(criteria).getSingleResult();
	}

	@Override
	public List<Profissional> buscarTodasEspecialidadesAtivas(ProfissionalFilter profissionalFilter) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();

		CriteriaQuery<Profissional> criteria = builder.createQuery(Profissional.class);
		Root<Profissional> root = criteria.from(Profissional.class);

		Predicate[] predicates = criarRestricoes(profissionalFilter, builder, root);

		criteria.where(predicates);
//		criteria.groupBy(root.get(Profissional_.especialidade), profissionalFilter.getEspecialidade());

		
		TypedQuery<Profissional> query = manager.createQuery(criteria);

		return query.getResultList();
	}
*/
}
