package br.com.receitaseguraapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.receitaseguraapi.model.general.Atestado;
import br.com.receitaseguraapi.model.general.Documento;
import br.com.receitaseguraapi.model.general.Receita;

@Repository
public interface DocumentoRepository extends JpaRepository<Documento, Integer> {

	@Query("from Atestado where id = :id and tipo = 'atestado'")
	Optional<Atestado> buscaAtestadoPorId(Integer id);

	@Query("from Documento where chaveAutenticacao = :chave")
	Optional<Documento> findDocumentoNumero(String chave);

	@Query("from Documento where autorizacao = :autorizacao")
	List<Atestado> buscaAtestadoPorAutorizacao(String autorizacao);

	@Query("from Receita where id = :id and tipo = 'receita_simples'")
	Optional<Receita> buscaReceitadoPorId(Integer id);

	@Query("from Documento where autorizacao = :autorizacao")
	List<Receita> buscaReceitaPorAutorizacao(String autorizacao);

	@Query("select count(a) from Documento a where tipo = 'atestado'")
	Integer totalAtestados();

	@Query("select count(a) from Documento a where tipo = 'receita_simples'")
	Integer totalReceitas();

	@Query("select a from Documento a where tipo = 'atestado'")
	List<Documento> listarTodosAtestados();

	@Query("select a from Documento a where tipo = 'receita_simples'")
	List<Documento> listarTodasReceitas();

	@Query("from Atestado where id = :id and tipo = 'atestado'")
	List<Atestado> bucasId(int id);

}
