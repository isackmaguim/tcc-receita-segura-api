package br.com.receitaseguraapi.repository.projection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResumoProfissional {

	private Integer id;
	private Integer tipoProfissional;
	private String nomeProfissional;
	private Integer statusProfissional;
	private String especialidade;
	private String instituicaoSaude;

	public ResumoProfissional(Integer id, Integer tipoProfissional, String nomeProfissional, Integer statusProfissional,
			String especialidade, String instituicaoSaude) {
		super();
		this.id = id;
		this.tipoProfissional = tipoProfissional;
		this.nomeProfissional = nomeProfissional;
		this.statusProfissional = statusProfissional;
		this.especialidade = especialidade;
		this.instituicaoSaude = instituicaoSaude;
	}

}
