package br.com.receitaseguraapi.resource.utils;

import java.io.InputStream;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

public class GeradorRelatorio {

	private InputStream inputStream;
	private Map<String, Object> parametros;
	private JRPdfExporter exporter;
	private JRBeanCollectionDataSource dados;

	public GeradorRelatorio(InputStream inputStreamRecebido, Map<String, Object> parametros, JRBeanCollectionDataSource dados) {
		this.inputStream = inputStreamRecebido;
		this.parametros = parametros;
		this.dados = dados;
	}
	
	public byte[] exportaRelatorio() throws JRException {

		try {

			JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parametros, dados);

			exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			
		} catch (JRException e) {
			throw new RuntimeException(e);
		}
		
		return null;

	}

}
