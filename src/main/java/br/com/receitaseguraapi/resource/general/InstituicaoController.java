package br.com.receitaseguraapi.resource.general;

import static br.com.receitaseguraapi.utils.ConstantesPaths.INSTITUICOES;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.receitaseguraapi.model.general.Instituicao;
import br.com.receitaseguraapi.services.InstituicaoService;
import br.com.receitaseguraapi.utils.ConstantesTags;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Created by Wendell Clive Santos de Lira - Email: wendell.clive@gmail.com
 * Data: 14/09/2018
 */

// Incluir anotations para compor a camada End-Poin REST
@RestController
@Api(value = INSTITUICOES, produces = MediaType.APPLICATION_JSON_VALUE, tags = { ConstantesTags.TAG_INSTITUICAO })
@RequestMapping(value = INSTITUICOES, produces = MediaType.APPLICATION_JSON_VALUE)
public class InstituicaoController {

	@Autowired
	private InstituicaoService service;


	@ApiOperation(value = "Busca Instituicao por id", notes = "Endpoint para CONSULTAR Instituicao por ID.", response = Instituicao.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_INSTITUICAO')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Instituicao> find(@PathVariable Integer id) throws ObjectNotFoundException {

		Instituicao obj = service.find(id);
		return ResponseEntity.ok().body(obj);

	}

	// Método para chamar Servico de inserir objeto
	@ApiOperation(value = "Insere Instituicao", notes = "Endpoint para INSERIR Instituicao.", response = URI.class)
	@PreAuthorize("hasAnyAuthority('ROLE_CADASTRA_INSTITUICAO')")
	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody Instituicao instituicao) {

		Instituicao instituicaoSalvo = service.insert(instituicao);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(instituicaoSalvo.getId()).toUri();
		return ResponseEntity.created(uri).build();

	}

	@ApiOperation(value = "Atualiza Instituição", notes = "Endpoint para ATUALIZAR Instituicao.", response = ResponseEntity.class)
	@PreAuthorize("hasAnyAuthority('ROLE_EDITA_INSTITUICAO')")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Instituicao> update(@Valid @RequestBody Instituicao obj, @PathVariable Integer id) {
		
		obj = service.update(id, obj);
		return ResponseEntity.ok().build();

	}


	@ApiOperation(value = "Remove Instituição", notes = "Endpoint para REMOVER Instituição por Id.", response = ResponseEntity.class)
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Não é possível excluir um Instituicao que tem Servidor criado"),
			@ApiResponse(code = 404, message = "Código inexistente") })
	@PreAuthorize("hasAnyAuthority('ROLE_REMOVE_INSTITUICAO')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {

		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Retorna Instituição até 10 linhas por pagina", notes = "Endpoint para CONSULTAR Instituição paginado.", response = Instituicao.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_INSTITUICAO')")
	@GetMapping(value = "/page")
	public ResponseEntity<Page<Instituicao>> findPage(
			@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "razaoSocial", defaultValue = "") String razaoSocial,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction,
			@RequestParam(value = "orderBy", defaultValue = "id") String orderBy) {

		Page<Instituicao> list = service.findPage(id, razaoSocial, page, linesPerPage, direction, orderBy);
		return ResponseEntity.ok().body(list);
	}

	@ApiOperation(value = "Retorna todos as Instituicoes de ", notes = "Endpoint para CONSULTAR todos as Instituicoes de .", response = Instituicao.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_INSTITUICAO')")
	@GetMapping
	public ResponseEntity<List<Instituicao>> findAll() {

		List<Instituicao> list = service.listarTodas();

		return ResponseEntity.ok().body(list);

	}
	

}
