package br.com.receitaseguraapi.resource.general;

import static br.com.receitaseguraapi.utils.ConstantesPaths.MEDICOS;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.receitaseguraapi.model.general.Medico;
import br.com.receitaseguraapi.model.general.Pessoa;
import br.com.receitaseguraapi.services.MedicoService;
import br.com.receitaseguraapi.utils.ConstantesTags;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@Api(value = MEDICOS, produces = MediaType.APPLICATION_JSON_VALUE, tags = { ConstantesTags.TAG_MEDICO })
@RequestMapping(value = MEDICOS, produces = MediaType.APPLICATION_JSON_VALUE)
public class MedicoController {

	@Autowired
	private MedicoService service;


	@ApiOperation(value = "Busca Medico por id", notes = "Endpoint para CONSULTAR Medico por ID.", response = Medico.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_MEDICO')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Medico> find(@PathVariable Integer id) throws ObjectNotFoundException {

		Medico obj = service.find(id);
		return ResponseEntity.ok().body(obj);

	}

	@ApiOperation(value = "Insere Medico", notes = "Endpoint para INSERIR Medico.", response = URI.class)
	@PreAuthorize("hasAnyAuthority('ROLE_CADASTRA_MEDICO')")
	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody Medico medico) {

		Medico medicoSalvo = service.insert(medico);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(medicoSalvo.getId()).toUri();
		return ResponseEntity.created(uri).build();

	}

	@ApiOperation(value = "Atualiza Medico", notes = "Endpoint para ATUALIZAR Medico.", response = ResponseEntity.class)
	@PreAuthorize("hasAnyAuthority('ROLE_EDITA_MEDICO')")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Pessoa> update(@Valid @RequestBody Pessoa obj, @PathVariable Integer id) {
		
		obj = service.update(id, obj);
		return ResponseEntity.ok().build();

	}


	@ApiOperation(value = "Remove Medico", notes = "Endpoint para REMOVER Medico por Id.", response = ResponseEntity.class)
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Não é possível excluir um Medico"),
			@ApiResponse(code = 404, message = "Código inexistente") })
	@PreAuthorize("hasAnyAuthority('ROLE_REMOVE_MEDICO')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {

		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Retorna Medico até 10 linhas por pagina", notes = "Endpoint para CONSULTAR Medico paginado.", response = Medico.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_MEDICO')")
	@GetMapping(value = "/page")
	public ResponseEntity<Page<Medico>> findPage(
			@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "razaoSocial", defaultValue = "") String razaoSocial,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction,
			@RequestParam(value = "orderBy", defaultValue = "id") String orderBy) {

		Page<Medico> list = service.findPage(id, razaoSocial, page, linesPerPage, direction, orderBy);
		return ResponseEntity.ok().body(list);
	}

	@ApiOperation(value = "Retorna todos as Instituicoes de ", notes = "Endpoint para CONSULTAR todos as Instituicoes de .", response = Pessoa.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_MEDICO')")
	@GetMapping
	public ResponseEntity<List<Medico>> findAll() {

		List<Medico> list = service.listarTodas();

		return ResponseEntity.ok().body(list);

	}
	

}
