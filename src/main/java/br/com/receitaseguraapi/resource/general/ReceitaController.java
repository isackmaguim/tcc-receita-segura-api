package br.com.receitaseguraapi.resource.general;

import static br.com.receitaseguraapi.utils.ConstantesPaths.RECEITAS;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.receitaseguraapi.model.general.Atestado;
import br.com.receitaseguraapi.model.general.Documento;
import br.com.receitaseguraapi.model.general.Instituicao;
import br.com.receitaseguraapi.model.general.Receita;
import br.com.receitaseguraapi.services.ReceitaService;
import br.com.receitaseguraapi.services.exceptions.ObjectNotFoundException;
import br.com.receitaseguraapi.utils.ConstantesTags;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = RECEITAS, produces = MediaType.APPLICATION_JSON_VALUE, tags = { ConstantesTags.TAG_RECEITA })
@RequestMapping(value = RECEITAS, produces = MediaType.APPLICATION_JSON_VALUE)
public class ReceitaController {

	@Autowired
	private ReceitaService service;

	@ApiOperation(value = "Busca Receita Beneficiario por id", notes = "Endpoint para CONSULTAR Receita Beneficiario por ID.", response = Receita.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_RECEITA')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Receita> buscaReceita(@PathVariable Integer id) {

		Receita obj = service.findById(id);
		return ResponseEntity.ok().body(obj);

	}

	@ApiOperation(value = "Insere Receita Beneficiario", notes = "Endpoint para INSERIR Receita Beneficiario.", response = URI.class)
	@PreAuthorize("hasAnyAuthority('ROLE_ADMINISTRADOR', 'ROLE_CADASTRA_RECEITA')")
	@PostMapping
	public ResponseEntity<Receita> inserir(@RequestBody Receita documento) throws ObjectNotFoundException {

		Receita documentoSalvo = service.insertReceita(documento);

		return ResponseEntity.ok().body(documentoSalvo);

	}

	@ApiOperation(value = "Impressão de Receita Beneficiario de Cadastro Individual do TitularDTO", notes = "Endpoint para IMPRIMIR Receita Beneficiario Individual do TitularDTO.", response = ResponseEntity.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_RECEITA')")
	@GetMapping(value = "/imprimirReceita/{autorizacao}")
	public ResponseEntity<byte[]> imprimirReceita(@PathVariable String autorizacao) throws Exception {

		byte[] relatorio = service.imprimirReceita(autorizacao);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE).body(relatorio);

	}

	@ApiOperation(value = "Retorna total de Atestados", notes = "Endpoint para Retornar as total de Atestados.", response = Instituicao.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_RECEITA')")
	@GetMapping(value = "/totalReceitas")
	public ResponseEntity<Integer> totalReceitas() {

		Integer total = service.totalReceitas();

		return ResponseEntity.ok().body(total);

	}
	
	@ApiOperation(value = "Retorna todos os Atestados", notes = "Endpoint para CONSULTAR todos os Atestados.", response = Atestado.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_RECEITA')")
	@GetMapping
	public ResponseEntity<List<Documento>> findAll() {

		List<Documento> list = service.listarTodasReceitas();

		return ResponseEntity.ok().body(list);

	}
}
