package br.com.receitaseguraapi.resource.general;

import static br.com.receitaseguraapi.utils.ConstantesPaths.PACIENTES;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.receitaseguraapi.model.general.Paciente;
import br.com.receitaseguraapi.model.general.Pessoa;
import br.com.receitaseguraapi.services.PacienteService;
import br.com.receitaseguraapi.utils.ConstantesTags;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@Api(value = PACIENTES, produces = MediaType.APPLICATION_JSON_VALUE, tags = { ConstantesTags.TAG_PACIENTE})
@RequestMapping(value = PACIENTES, produces = MediaType.APPLICATION_JSON_VALUE)
public class PacienteController {

	@Autowired
	private PacienteService service;


	@ApiOperation(value = "Busca Paciente por id", notes = "Endpoint para CONSULTAR Paciente por ID.", response = Pessoa.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_PACIENTE')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Paciente> find(@PathVariable Integer id) throws ObjectNotFoundException {

		Paciente obj = service.find(id);
		return ResponseEntity.ok().body(obj);

	}

	@ApiOperation(value = "Insere Paciente", notes = "Endpoint para INSERIR Paciente.", response = URI.class)
	@PreAuthorize("hasAnyAuthority('ROLE_CADASTRA_PACIENTE')")
	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody Paciente paciente) {

		Paciente pacienteSalvo = service.insert(paciente);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pacienteSalvo.getId()).toUri();
		return ResponseEntity.created(uri).build();

	}

	@ApiOperation(value = "Atualiza Paciente", notes = "Endpoint para ATUALIZAR Paciente.", response = ResponseEntity.class)
	@PreAuthorize("hasAnyAuthority('ROLE_EDITA_PACIENTE')")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Paciente> update(@Valid @RequestBody Paciente obj, @PathVariable Integer id) {
		
		obj = service.update(id, obj);
		return ResponseEntity.ok().build();

	}


	@ApiOperation(value = "Remove Paciente", notes = "Endpoint para REMOVER Paciente por Id.", response = ResponseEntity.class)
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Não é possível excluir uma Paciente"),
			@ApiResponse(code = 404, message = "Código inexistente") })
	@PreAuthorize("hasAnyAuthority('ROLE_REMOVE_PACIENTE')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {

		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Retorna Paciente até 10 linhas por pagina", notes = "Endpoint para CONSULTAR Pessoa paginado.", response = Paciente.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_PACIENTE')")
	@GetMapping(value = "/page")
	public ResponseEntity<Page<Paciente>> findPage(
			@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "razaoSocial", defaultValue = "") String razaoSocial,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction,
			@RequestParam(value = "orderBy", defaultValue = "id") String orderBy) {

		Page<Paciente> list = service.findPage(id, razaoSocial, page, linesPerPage, direction, orderBy);
		return ResponseEntity.ok().body(list);
	}

	@ApiOperation(value = "Retorna todos os Pacientes", notes = "Endpoint para CONSULTAR todos os Pacientes", response = Paciente.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_PACIENTE')")
	@GetMapping
	public ResponseEntity<List<Paciente>> findAll() {

		List<Paciente> list = service.listarTodas();

		return ResponseEntity.ok().body(list);

	}
	

}
