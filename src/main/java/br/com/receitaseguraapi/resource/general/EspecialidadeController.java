package br.com.receitaseguraapi.resource.general;

import static br.com.receitaseguraapi.utils.ConstantesPaths.ESPECIALIDADES;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.receitaseguraapi.model.general.Especialidade;
import br.com.receitaseguraapi.services.EspecialidadeService;
import br.com.receitaseguraapi.utils.ConstantesTags;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Created by Wendell Clive Santos de Lira - Email: wendell.clive@gmail.com
 * Data: 14/09/2018
 */

// Incluir anotations para compor a camada End-Poin REST
@RestController
@Api(value = ESPECIALIDADES, produces = MediaType.APPLICATION_JSON_VALUE, tags = { ConstantesTags.TAG_ESPECIALIDADE })
@RequestMapping(value = ESPECIALIDADES, produces = MediaType.APPLICATION_JSON_VALUE)
public class EspecialidadeController {

	@Autowired
	private EspecialidadeService service;

	@ApiOperation(value = "Busca Especialidade por id", notes = "Endpoint para CONSULTAR Especialidade por ID.", response = Especialidade.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ESPECIALIDADE')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Especialidade> find(@PathVariable Integer id) {
		Especialidade obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}

	@ApiOperation(value = "Insere Especialidade", notes = "Endpoint para INSERIR Especialidade.", response = URI.class)
	@PreAuthorize("hasAnyAuthority('ROLE_CADASTRA_ESPECIALIDADE')")
	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody Especialidade especialidade) {
		Especialidade especialidadeSalvo = service.insert(especialidade);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(especialidadeSalvo.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@ApiOperation(value = "Atualiza Especialidade", notes = "Endpoint para ATUALIZAR Especialidade.", response = ResponseEntity.class)
	@PreAuthorize("hasAnyAuthority('ROLE_EDITA_ESPECIALIDADE')")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Especialidade> update(@Valid @RequestBody Especialidade obj, @PathVariable Integer id) {
		obj = service.update(id, obj);
		return ResponseEntity.ok().build();
	}


	@ApiOperation(value = "Remove Especialidade", notes = "Endpoint para REMOVER Especialidade por Id.", response = ResponseEntity.class)
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Não é possível excluir um Especialidade que tem Servidor criado"),
			@ApiResponse(code = 404, message = "Código inexistente") })
	@PreAuthorize("hasAnyAuthority('ROLE_REMOVE_ESPECIALIDADE')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Retorna todos os Limites de Consultas por Especialidades", notes = "Endpoint para CONSULTAR todos os Limites de Consultas por Especialidades.", response = Especialidade.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ESPECIALIDADE')")
	@GetMapping
	public ResponseEntity<List<Especialidade>> findAll() {
		List<Especialidade> list = service.listarTodas();
		return ResponseEntity.ok().body(list);
	}

	@ApiOperation(value = "Retorna Limites de Consultas por Especialidades até 10 linhas por pagina", notes = "Endpoint para CONSULTAR Limites de Consultas por Especialidades paginado.", response = Especialidade.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ESPECIALIDADE')")
	@GetMapping(value = "/page")
	public ResponseEntity<Page<Especialidade>> findPage(
			@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "nomeEspecialidade", defaultValue = "") String nomeEspecialidade,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction,
			@RequestParam(value = "orderBy", defaultValue = "id") String orderBy) {
		Page<Especialidade> list = service.findPage(id, nomeEspecialidade, page, linesPerPage, direction, orderBy);
		return ResponseEntity.ok().body(list);
	}

}
