package br.com.receitaseguraapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.receitaseguraapi.model.general.Especialidade;
import br.com.receitaseguraapi.repository.EspecialidadeRepository;
import br.com.receitaseguraapi.repository.filtros.ProfissionalFilter;
import br.com.receitaseguraapi.services.exceptions.DataIntegrityException;
import br.com.receitaseguraapi.services.exceptions.ObjectNotFoundException;

@Service
public class EspecialidadeService {

	@Autowired
	private EspecialidadeRepository reposit;

	public Especialidade find(Integer id) {

		Optional<Especialidade> obj = reposit.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Especialidade.class.getName()));

	}

	@Transactional
	public Especialidade insert(Especialidade especialidade) {

		especialidade.setId(null); // faz o método entender que se não houver ID então é uma alteração
		return reposit.save(especialidade);

	}

	public Especialidade update(Integer id, Especialidade especialidade) {

		Especialidade especialidadeSalvo = find(id);

		BeanUtils.copyProperties(especialidade, especialidadeSalvo, "id", "limites");
		return reposit.save(especialidadeSalvo);

	}

	public void delete(Integer id) {

		find(id);
		try {
			reposit.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel Excluir um Especialidade");
		}
	}

	public List<Especialidade> listarTodas() {
		return reposit.findAll();
	}

	public Page<Especialidade> findPage(String id, String nomeEspecialidade, Integer page, Integer linesPerPage,
			String direction, String orderBy) {

		Pageable pageable = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return reposit.findByEspecialidade(id, nomeEspecialidade, pageable);
	}

	public Especialidade buscarVagasTotais(String especialidade) {

		Optional<Especialidade> obj = reposit.buscarVagasTotais(especialidade);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + especialidade + ", Tipo: " + Especialidade.class.getName()));
 
	}

	public List<Especialidade> buscarTodasEspecialidadesAtivas(ProfissionalFilter profissionalFilter) {

		String tipo = profissionalFilter.getTipoProfissional();
		String status = profissionalFilter.getStatusProfissional();
		String classificacao = profissionalFilter.getClassificacao();

		return reposit.buscarEspecialidadesAtivas(tipo, status, classificacao);
	}
	
}
