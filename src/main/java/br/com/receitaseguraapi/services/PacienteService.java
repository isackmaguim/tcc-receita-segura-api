package br.com.receitaseguraapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.receitaseguraapi.model.general.Endereco;
import br.com.receitaseguraapi.model.general.Paciente;
import br.com.receitaseguraapi.model.general.Pessoa;
import br.com.receitaseguraapi.repository.EnderecoRepository;
import br.com.receitaseguraapi.repository.PessoaRepository;
import br.com.receitaseguraapi.services.exceptions.DataIntegrityException;
import br.com.receitaseguraapi.services.exceptions.ObjectNotFoundException;

@Service
public class PacienteService {

	@Autowired
	private PessoaRepository pessoaRepository;

	@Autowired
	private EnderecoRepository enderecoRepository;

	public Paciente find(Integer id) {

		Optional<Paciente> obj = pessoaRepository.buscarPorPacientePorId(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Pessoa.class.getName()));

	}

	@Transactional
	public Paciente insert(Paciente obj) {	
		
		obj.setId(null); 
		Endereco end = new Endereco(obj.getEndereco().getLogradouro(), obj.getEndereco().getBairro(),
			obj.getEndereco().getCep(), obj.getEndereco().getCidade(), obj.getEndereco().getUf(),
			obj.getEndereco().getNumeroEndereco(), obj.getEndereco().getComplemento(), null, obj.getEndereco().getPessoa());
		
		obj.setEndereco(end);
		pessoaRepository.save(obj);
		enderecoRepository.save(end);

	return obj;

	}

	public Paciente update(Integer id, Paciente pessoa) {

		Paciente pessoaSalvo = find(id);
		BeanUtils.copyProperties(pessoa, pessoaSalvo, "id", "servicos");
		return pessoaRepository.save(pessoaSalvo);

	}

	public void delete(Integer id) {

		find(id);
		try {
			pessoaRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel Excluir um Médico");
		}
	}

	public Page<Paciente> findPage(String id, String razaoSocial, Integer page, Integer linesPerPage, String direction,
			String orderBy) {

		Pageable pageable = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);

		return pessoaRepository.buscarTodosPacientesPaginado(pageable);
	}

	public List<Paciente> listarTodas() {

		return pessoaRepository.buscarTodosPacientes();

	}

}
