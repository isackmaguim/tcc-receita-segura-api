package br.com.receitaseguraapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.receitaseguraapi.model.general.Cid;
import br.com.receitaseguraapi.repository.CidRepository;
import br.com.receitaseguraapi.services.exceptions.ObjectNotFoundException;


@Service
public class CidService {

	@Autowired
	private CidRepository repository;

	public List<Cid> listarTodos() {

		return repository.findAll();

	}

	public Cid gravar(Cid obj) {

		obj.setId(null);
		if (obj.getId() != null) {
			Cid objSalva = repository.save(obj);
			return objSalva;
		}
		return null;

	}

	public Cid buscarPorId(Integer id) throws ObjectNotFoundException {

		Optional<Cid> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Cid.class.getName()));

	}

	public void removerPorId(Integer id) {

		repository.deleteById(id);

	}

	public Cid atualizarPorId(Cid obj, Integer id) throws ObjectNotFoundException {

		Cid salvo = buscarPorId(id);
		BeanUtils.copyProperties(obj, salvo);
		return repository.save(salvo);

	}

	public Page<Cid> findPage(String id, Integer page, Integer linesPerPage, String direction, String orderBy) {

		Pageable pageable = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repository.findAll(pageable);

	}

}
