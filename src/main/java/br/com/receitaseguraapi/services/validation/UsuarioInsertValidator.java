package br.com.receitaseguraapi.services.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.receitaseguraapi.model.dto.usuario.UsuarioNewDTO;
import br.com.receitaseguraapi.model.usuario.Usuario;
import br.com.receitaseguraapi.repository.UsuarioRepository;
import br.com.receitaseguraapi.resource.general.exceptions.FieldMessage;

public class UsuarioInsertValidator implements ConstraintValidator<UsuarioInsert, UsuarioNewDTO> {

	@Autowired
	private UsuarioRepository reposit;
	
	@Override
	public void initialize(UsuarioInsert ann) {
	}

	@Override
	public boolean isValid(UsuarioNewDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();

		if (objDto.getStatusUsuario() == null) {
			list.add(new FieldMessage("statusUsuario", "Status não pode ser nulo"));
		}
		
		Optional<Usuario> aux = reposit.findByMatricula(objDto.getMatricula());
		if (aux != null) {
			list.add(new FieldMessage("email", "Email já existente!!"));
		}
		
		// inclua os testes aqui, inserindo erros na lista
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}