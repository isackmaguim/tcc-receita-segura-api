package br.com.receitaseguraapi.utils;

import java.time.LocalDate;

public class CalculaIdade {

	public Integer Idade(LocalDate dataNascimento) {
		
		LocalDate hoje = LocalDate.now();

		int idade = hoje.getYear() - dataNascimento.getYear();

		if (hoje.getMonthValue() < dataNascimento.getMonthValue()) {
			idade--;
		} else {
			if (hoje.getMonthValue() == dataNascimento.getMonthValue()
					&& hoje.getDayOfMonth() < dataNascimento.getDayOfMonth()) {
				idade--;
			}
		}
		return idade;
	}
}