package br.com.receitaseguraapi.utils;

import java.util.Random;

public class GeraAutorizacao {

	private static Random rand = new Random();
	private static char[] letras = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz123456789".toCharArray();

	public static String nomeAleatorio (int nCaracteres) {
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i <= nCaracteres; i++) {
	        int ch = rand.nextInt (letras.length);
	        sb.append (letras [ch]);
	    }    
	    return sb.toString();    
	}
	
}
