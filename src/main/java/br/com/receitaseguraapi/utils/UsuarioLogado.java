package br.com.receitaseguraapi.utils;

import br.com.receitaseguraapi.security.UserSS;
import br.com.receitaseguraapi.services.UserService;

public class UsuarioLogado {

	public static Integer logado() {
		UserSS user = UserService.authenticated();
		return user.getId();
	}
	
	public static UserSS logadoUser() {
		UserSS user = UserService.authenticated();
		return user;
	}

}
