package br.com.receitaseguraapi.utils;

/***
 * 
 * Created by Wendell Clive - email: wendell.clive@gmail.com
 * 07/06/2018
 *
 * */

public class ConstantesPaths {

	public static final String PATH_API = "/api";
	public static final String RAIZ = PATH_API + "/";
	public static final String TOKENS = PATH_API + "/tokens";
	public static final String ENDERECOS = PATH_API + "/enderecos";
	public static final String USUARIOS = PATH_API + "/usuarios";
	public static final String ESPECIALIDADES = PATH_API + "/especialidades";
	public static final String INSTITUICOES = PATH_API + "/instituicoes";
	public static final String CIDS = PATH_API + "/cids";
	public static final String MEDICAMENTOS = PATH_API + "/medicamentos";
	public static final String ATESTADOS = PATH_API + "/atestados";
	public static final String RECEITAS = PATH_API + "/receitas";
	public static final String MEDICOS = PATH_API + "/medicos";
	public static final String PACIENTES = PATH_API + "/pacientes";
	
}