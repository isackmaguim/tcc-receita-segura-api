package br.com.receitaseguraapi.utils;

/***
 * 
 * Created by Wendell Clive - email: wendell.clive@gmail.com
 * 19/08/2018
 *
 * */
public class ConstantesTags {
	
	public static final String TAG_TOKEN = "TOKEN";
	public static final String TAG_ENDERECO = "ENDEREÇO";
	public static final String TAG_USUARIO = "USUARIO";
	public static final String TAG_ESPECIALIDADE = "ESPECIALIDADE";
	public static final String TAG_INSTITUICAO = "INSTITUICAO";
	public static final String TAG_CID = "CID";
	public static final String TAG_MEDICAMENTO = "MEDICAMENTO";
	public static final String TAG_RECEITA = "RECEITA";
	public static final String TAG_ATESTADO = "ATESTADO";
	public static final String TAG_MEDICO = "MEDICO";
	public static final String TAG_PACIENTE = "PACIENTE";

}